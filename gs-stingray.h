//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_GS_STINGRAY_H
#define LIBRARY_GS_STINGRAY_GS_STINGRAY_H

#include <gs-stingray/stgmain.h>
#include <gs-stingray/stgschema.h>
#include <gs-stingray/stgjsebuilder.h>
#include <gs-stingray/stgjsegenerator.h>
#include <gs-stingray/stgjsereader.h>
#include <gs-stingray/stgjseparser.h>
#include <gs-stingray/stgsoelogger.h>
#include <gs-stingray/stgsoemessage.h>
#include <gs-stingray/stgsoesession.h>
#include <gs-stingray/stgavhservicebrowser.h>
#include <gs-stingray/stgavhserviceresolver.h>
#include <gs-stingray/stginit.h>

#endif //LIBRARY_GS_STINGRAY_GS_STINGRAY_H
