//
// Created by root on 13.11.2021.
//

#include "stgavhservicebrowser.h"

enum {
    STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND = 1,
    STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED,
    _STG_AVH_SERVICE_BROWSER_SIGNAL_COUNT
};

guint stg_avh_service_browser_signals[_STG_AVH_SERVICE_BROWSER_SIGNAL_COUNT] = {0};

void stg_avh_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags);
void _stg_avh_service_browser_endpoint_found(STGAVHServiceBrowser *self, STGEndpoint *endpoint);
void _stg_avh_service_browser_endpoint_removed(STGAVHServiceBrowser *self, gchar *name);
void stg_avh_service_browser_avh_s_service_resolver_event(AVHSServiceResolver *sender, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self);

G_DEFINE_TYPE(STGAVHServiceBrowser, stg_avh_service_browser, AVH_TYPE_S_SERVICE_BROWSER)

static void stg_avh_service_browser_class_init(STGAVHServiceBrowserClass *class) {
    AVH_S_SERVICE_BROWSER_CLASS(class)->event = stg_avh_service_browser_event;

    class->endpoint_found = _stg_avh_service_browser_endpoint_found;
    class->endpoint_removed = _stg_avh_service_browser_endpoint_removed;

    stg_avh_service_browser_signals[STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND] = g_signal_new("endpoint-found", STG_TYPE_AVH_SERVICE_BROWSER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(STGAVHServiceBrowserClass, endpoint_found), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
    stg_avh_service_browser_signals[STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED] = g_signal_new("endpoint-removed", STG_TYPE_AVH_SERVICE_BROWSER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(STGAVHServiceBrowserClass, endpoint_removed), NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_POINTER);
}

static void stg_avh_service_browser_init(STGAVHServiceBrowser *self) {

}

void stg_avh_service_browser_event(AVHSServiceBrowser *self, AvahiSServiceBrowser *browser, AvahiIfIndex interface, AvahiProtocol protocol, AvahiBrowserEvent event, gchar *name, gchar *type, gchar *domain, AvahiLookupResultFlags flags) {
    if (event == AVAHI_BROWSER_NEW) {
        STGAVHServiceResolver *resolver = stg_avh_service_resolver_new(name, type, NULL);
        (void)g_signal_connect(resolver, "event", G_CALLBACK(stg_avh_service_browser_avh_s_service_resolver_event), self);
    } else if (event == AVAHI_BROWSER_REMOVE) {
        stg_avh_service_browser_endpoint_removed(STG_AVH_SERVICE_BROWSER(self), name);
    }
}

void stg_avh_service_browser_endpoint_found(STGAVHServiceBrowser *self, STGEndpoint *endpoint) {
    g_signal_emit(self, stg_avh_service_browser_signals[STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_FOUND], 0, endpoint);
}

void _stg_avh_service_browser_endpoint_found(STGAVHServiceBrowser *self, STGEndpoint *endpoint) {
    g_print("endpoint-found\n");
}

void stg_avh_service_browser_endpoint_removed(STGAVHServiceBrowser *self, gchar *name) {
    g_signal_emit(self, stg_avh_service_browser_signals[STG_AVH_SERVICE_BROWSER_SIGNAL_ENDPOINT_REMOVED], 0, name);
}

void _stg_avh_service_browser_endpoint_removed(STGAVHServiceBrowser *self, gchar *name) {
    g_print("endpoint-removed\n");
}

void stg_avh_service_browser_avh_s_service_resolver_event(AVHSServiceResolver *sender, AvahiSServiceResolver *resolver, AvahiIfIndex interface, AvahiProtocol protocol, AvahiResolverEvent event, gchar *name, gchar *type, gchar *domain, gchar *host, AvahiAddress *address, guint16 port, AvahiStringList *txt, AvahiLookupResultFlags flags, gpointer self) {
    if (event == AVAHI_RESOLVER_FOUND) {
        gchar ip[AVAHI_ADDRESS_STR_MAX] = {0};
        (void)avahi_address_snprint(ip, AVAHI_ADDRESS_STR_MAX, address);

        STGEndpoint endpoint = {0};
        endpoint.name = name;
        endpoint.ip = ip;
        endpoint.port = port;

        for (AvahiStringList *entry = txt; entry != NULL; entry = entry->next) {
            if (g_str_has_prefix((gchar *)entry->text, "model=")) {
                endpoint.model = (gchar *)entry->text + 6;
            } else if (g_str_has_prefix((gchar *)entry->text, "txtvers=")) {
                endpoint.version = (gchar *)entry->text + 8;
            }
        }

        stg_avh_service_browser_endpoint_found(self, &endpoint);
    }

    g_object_unref(sender);
}

STGAVHServiceBrowser *stg_avh_service_browser_new(gchar *type, GError **error) {
    g_autoptr(STGAVHServiceBrowser) self = g_object_new(STG_TYPE_AVH_SERVICE_BROWSER, NULL);

    AVHServerDefault *server = NULL;
    if ((server = avh_server_default_shared(error)) == NULL) return NULL;

    g_autoptr(AvahiSServiceBrowser) object = NULL;
    if ((object = avh_s_service_browser_new(AVH_SERVER(server)->object, AVAHI_IF_UNSPEC, AVAHI_PROTO_INET, type, "local", 0, avh_s_service_browser_event_callback, self, error)) == NULL) return NULL;

    AVH_S_SERVICE_BROWSER(self)->object = g_steal_pointer(&object);

    return g_steal_pointer(&self);
}
