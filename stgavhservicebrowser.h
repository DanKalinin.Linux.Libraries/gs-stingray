//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGAVHSERVICEBROWSER_H
#define LIBRARY_GS_STINGRAY_STGAVHSERVICEBROWSER_H

#include "stgmain.h"
#include "stgschema.h"
#include "stgavhserviceresolver.h"

G_BEGIN_DECLS

#define STG_TYPE_AVH_SERVICE_BROWSER stg_avh_service_browser_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGAVHServiceBrowser, stg_avh_service_browser, STG, AVH_SERVICE_BROWSER, AVHSServiceBrowser)

struct _STGAVHServiceBrowserClass {
    AVHSServiceBrowserClass super;

    void (*endpoint_found)(STGAVHServiceBrowser *self, STGEndpoint *endpoint);
    void (*endpoint_removed)(STGAVHServiceBrowser *self, gchar *name);
};

struct _STGAVHServiceBrowser {
    AVHSServiceBrowser super;
};

void stg_avh_service_browser_endpoint_found(STGAVHServiceBrowser *self, STGEndpoint *endpoint);
void stg_avh_service_browser_endpoint_removed(STGAVHServiceBrowser *self, gchar *name);

STGAVHServiceBrowser *stg_avh_service_browser_new(gchar *type, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGAVHSERVICEBROWSER_H
