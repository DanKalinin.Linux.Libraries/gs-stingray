//
// Created by root on 13.11.2021.
//

#include "stgavhserviceresolver.h"

G_DEFINE_TYPE(STGAVHServiceResolver, stg_avh_service_resolver, AVH_TYPE_S_SERVICE_RESOLVER)

static void stg_avh_service_resolver_class_init(STGAVHServiceResolverClass *class) {

}

static void stg_avh_service_resolver_init(STGAVHServiceResolver *self) {

}

STGAVHServiceResolver *stg_avh_service_resolver_new(gchar *name, gchar *type, GError **error) {
    g_autoptr(STGAVHServiceResolver) self = g_object_new(STG_TYPE_AVH_SERVICE_RESOLVER, NULL);

    AVHServerDefault *server = NULL;
    if ((server = avh_server_default_shared(error)) == NULL) return NULL;

    g_autoptr(AvahiSServiceResolver) object = NULL;
    if ((object = avh_s_service_resolver_new(AVH_SERVER(server)->object, AVAHI_IF_UNSPEC, AVAHI_PROTO_INET, name, type, "local", AVAHI_PROTO_INET, 0, avh_s_service_resolver_event_callback, self, error)) == NULL) return NULL;

    AVH_S_SERVICE_RESOLVER(self)->object = g_steal_pointer(&object);

    return g_steal_pointer(&self);
}
