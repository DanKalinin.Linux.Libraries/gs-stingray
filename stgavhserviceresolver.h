//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGAVHSERVICERESOLVER_H
#define LIBRARY_GS_STINGRAY_STGAVHSERVICERESOLVER_H

#include "stgmain.h"

G_BEGIN_DECLS

#define STG_TYPE_AVH_SERVICE_RESOLVER stg_avh_service_resolver_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGAVHServiceResolver, stg_avh_service_resolver, STG, AVH_SERVICE_RESOLVER, AVHSServiceResolver)

struct _STGAVHServiceResolverClass {
    AVHSServiceResolverClass super;
};

struct _STGAVHServiceResolver {
    AVHSServiceResolver super;
};

STGAVHServiceResolver *stg_avh_service_resolver_new(gchar *name, gchar *type, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGAVHSERVICERESOLVER_H
