//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGINIT_H
#define LIBRARY_GS_STINGRAY_STGINIT_H

#include "stgmain.h"

G_BEGIN_DECLS

void stg_init(void);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGINIT_H
