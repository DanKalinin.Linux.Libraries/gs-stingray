//
// Created by root on 14.11.2021.
//

#include "stgjsebuilder.h"

JsonObject *stg_jse_builder_power_set(gboolean on) {
    gchar *value = on ? "on" : "off";

    JsonObject *self = json_object_new();
    json_object_set_string_member(self, "state", value);
    return self;
}

JsonObject *stg_jse_builder_channel_set(STGChannel *channel) {
    JsonObject *self = stg_jse_builder_channel(channel);
    return self;
}

JsonObject *stg_jse_builder_channel(STGChannel *channel) {
    JsonObject *self = json_object_new();
    json_object_set_string_member(self, "channelListId", channel->channel_list_id);
    json_object_set_int_member(self, "channelNumber", channel->channel_number);
    return self;
}
