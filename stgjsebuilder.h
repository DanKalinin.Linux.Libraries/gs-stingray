//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGJSEBUILDER_H
#define LIBRARY_GS_STINGRAY_STGJSEBUILDER_H

#include "stgmain.h"
#include "stgschema.h"

G_BEGIN_DECLS

JsonObject *stg_jse_builder_power_set(gboolean on);
JsonObject *stg_jse_builder_channel_set(STGChannel *channel);

JsonObject *stg_jse_builder_channel(STGChannel *channel);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGJSEBUILDER_H
