//
// Created by root on 14.11.2021.
//

#include "stgjsegenerator.h"

G_DEFINE_TYPE(STGJSEGenerator, stg_jse_generator, JSE_TYPE_GENERATOR)

static void stg_jse_generator_class_init(STGJSEGeneratorClass *class) {

}

static void stg_jse_generator_init(STGJSEGenerator *self) {

}

STGJSEGenerator *stg_jse_generator_new(JsonNode *root) {
    STGJSEGenerator *self = g_object_new(STG_TYPE_JSE_GENERATOR, "root", root, NULL);
    return self;
}
