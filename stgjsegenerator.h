//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGJSEGENERATOR_H
#define LIBRARY_GS_STINGRAY_STGJSEGENERATOR_H

#include "stgmain.h"

G_BEGIN_DECLS

#define STG_TYPE_JSE_GENERATOR stg_jse_generator_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGJSEGenerator, stg_jse_generator, STG, JSE_GENERATOR, JSEGenerator)

struct _STGJSEGeneratorClass {
    JSEGeneratorClass super;
};

struct _STGJSEGenerator {
    JSEGenerator super;
};

STGJSEGenerator *stg_jse_generator_new(JsonNode *root);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGJSEGENERATOR_H
