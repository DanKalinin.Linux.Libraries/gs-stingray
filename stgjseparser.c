//
// Created by root on 14.11.2021.
//

#include "stgjseparser.h"

G_DEFINE_TYPE(STGJSEParser, stg_jse_parser, JSE_TYPE_PARSER)

static void stg_jse_parser_class_init(STGJSEParserClass *class) {

}

static void stg_jse_parser_init(STGJSEParser *self) {

}

STGJSEParser *stg_jse_parser_new(void) {
    STGJSEParser *self = g_object_new(STG_TYPE_JSE_PARSER, NULL);
    return self;
}
