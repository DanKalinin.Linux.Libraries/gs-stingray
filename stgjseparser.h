//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGJSEPARSER_H
#define LIBRARY_GS_STINGRAY_STGJSEPARSER_H

#include "stgmain.h"

G_BEGIN_DECLS

#define STG_TYPE_JSE_PARSER stg_jse_parser_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGJSEParser, stg_jse_parser, STG, JSE_PARSER, JSEParser)

struct _STGJSEParserClass {
    JSEParserClass super;
};

struct _STGJSEParser {
    JSEParser super;
};

STGJSEParser *stg_jse_parser_new(void);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGJSEPARSER_H
