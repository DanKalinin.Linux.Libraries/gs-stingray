//
// Created by root on 14.11.2021.
//

#include "stgjsereader.h"

GList *stg_jse_reader_channels_get(JsonArray *self) {
    GList *ret = stg_jse_reader_channels(self);
    return ret;
}

STGChannel *stg_jse_reader_channel(JsonObject *self) {
    STGChannel ret = {0};
    ret.channel_list_id = (gchar *)json_object_get_string_member(self, "channelListId");
    ret.channel_number = json_object_get_int_member(self, "channelNumber");
    ret.channel_list_name = (gchar *)json_object_get_string_member(self, "channelListName");
    ret.channel_name = (gchar *)json_object_get_string_member(self, "channelName");
    ret.visible = json_object_get_boolean_member(self, "visible");
    return stg_channel_copy(&ret);
}

GList *stg_jse_reader_channels(JsonArray *self) {
    GList *ret = NULL;
    guint length = json_array_get_length(self);

    for (guint index = 0; index < length; index++) {
        JsonObject *object_channel = json_array_get_object_element(self, index);
        STGChannel *ret_channel = stg_jse_reader_channel(object_channel);
        ret = g_list_append(ret, ret_channel);
    }

    return ret;
}
