//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGJSEREADER_H
#define LIBRARY_GS_STINGRAY_STGJSEREADER_H

#include "stgmain.h"
#include "stgschema.h"

G_BEGIN_DECLS

GList *stg_jse_reader_channels_get(JsonArray *self);

STGChannel *stg_jse_reader_channel(JsonObject *self);
GList *stg_jse_reader_channels(JsonArray *self);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGJSEREADER_H
