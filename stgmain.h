//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGMAIN_H
#define LIBRARY_GS_STINGRAY_STGMAIN_H

#include <glib-ext/glib-ext.h>
#include <soup-ext/soup-ext.h>
#include <gnutls-ext/gnutls-ext.h>
#include <glib-networking-ext/glib-networking-ext.h>
#include <json-ext/json-ext.h>
#include <avahi-ext/avahi-ext.h>

#endif //LIBRARY_GS_STINGRAY_STGMAIN_H
