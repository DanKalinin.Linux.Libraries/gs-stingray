//
// Created by root on 13.11.2021.
//

#include "stgschema.h"










STGChannel *stg_channel_copy(STGChannel *self) {
    STGChannel *ret = g_new0(STGChannel, 1);
    stg_channel_set_channel_list_id(ret, self->channel_list_id);
    ret->channel_number = self->channel_number;
    stg_channel_set_channel_list_name(ret, self->channel_list_name);
    stg_channel_set_channel_name(ret, self->channel_name);
    ret->visible = self->visible;
    return ret;
}

void stg_channel_free(STGChannel *self) {
    stg_channel_set_channel_list_id(self, NULL);
    stg_channel_set_channel_list_name(self, NULL);
    stg_channel_set_channel_name(self, NULL);
    g_free(self);
}










STGEndpoint *stg_endpoint_copy(STGEndpoint *self) {
    STGEndpoint *ret = g_new0(STGEndpoint, 1);
    stg_endpoint_set_name(ret, self->name);
    stg_endpoint_set_ip(ret, self->ip);
    ret->port = self->port;
    stg_endpoint_set_model(ret, self->model);
    stg_endpoint_set_version(ret, self->version);
    return ret;
}

void stg_endpoint_free(STGEndpoint *self) {
    stg_endpoint_set_name(self, NULL);
    stg_endpoint_set_ip(self, NULL);
    stg_endpoint_set_model(self, NULL);
    stg_endpoint_set_version(self, NULL);
    g_free(self);
}
