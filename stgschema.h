//
// Created by root on 13.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGSCHEMA_H
#define LIBRARY_GS_STINGRAY_STGSCHEMA_H

#include "stgmain.h"










G_BEGIN_DECLS

typedef struct {
    gchar *channel_list_id;
    gint channel_number;
    gchar *channel_list_name;
    gchar *channel_name;
    gboolean visible;
} STGChannel;

GE_STRUCTURE_FIELD(stg_channel, channel_list_id, STGChannel, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(stg_channel, channel_list_name, STGChannel, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(stg_channel, channel_name, STGChannel, gchar, g_strdup, g_free, NULL)

STGChannel *stg_channel_copy(STGChannel *self);
void stg_channel_free(STGChannel *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(STGChannel, stg_channel_free)

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    gchar *name;
    gchar *ip;
    gint port;
    gchar *model;
    gchar *version;
} STGEndpoint;

GE_STRUCTURE_FIELD(stg_endpoint, name, STGEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(stg_endpoint, ip, STGEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(stg_endpoint, model, STGEndpoint, gchar, g_strdup, g_free, NULL)
GE_STRUCTURE_FIELD(stg_endpoint, version, STGEndpoint, gchar, g_strdup, g_free, NULL)

STGEndpoint *stg_endpoint_copy(STGEndpoint *self);
void stg_endpoint_free(STGEndpoint *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(STGEndpoint, stg_endpoint_free)

G_END_DECLS










#endif //LIBRARY_GS_STINGRAY_STGSCHEMA_H
