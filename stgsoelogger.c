//
// Created by root on 14.11.2021.
//

#include "stgsoelogger.h"

G_DEFINE_TYPE(STGSOELogger, stg_soe_logger, SOE_TYPE_LOGGER)

static void stg_soe_logger_class_init(STGSOELoggerClass *class) {

}

static void stg_soe_logger_init(STGSOELogger *self) {

}

STGSOELogger *stg_soe_logger_new(void) {
    STGSOELogger *self = g_object_new(STG_TYPE_SOE_LOGGER, "level", SOUP_LOGGER_LOG_BODY, "max-body-size", -1, NULL);
    return self;
}
