//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGSOELOGGER_H
#define LIBRARY_GS_STINGRAY_STGSOELOGGER_H

#include "stgmain.h"

G_BEGIN_DECLS

#define STG_TYPE_SOE_LOGGER stg_soe_logger_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGSOELogger, stg_soe_logger, STG, SOE_LOGGER, SOELogger)

struct _STGSOELoggerClass {
    SOELoggerClass super;
};

struct _STGSOELogger {
    SOELogger super;
};

STGSOELogger *stg_soe_logger_new(void);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGSOELOGGER_H
