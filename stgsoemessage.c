//
// Created by root on 14.11.2021.
//

#include "stgsoemessage.h"

gboolean stg_soe_message_set_error(STGSOEMessage *self, GError **error);

G_DEFINE_TYPE(STGSOEMessage, stg_soe_message, SOE_TYPE_MESSAGE)

static void stg_soe_message_class_init(STGSOEMessageClass *class) {

}

static void stg_soe_message_init(STGSOEMessage *self) {

}

gboolean stg_soe_message_set_error(STGSOEMessage *self, GError **error) {
    soe_http_set_error(error, SOUP_MESSAGE(self)->status_code);
    return TRUE;
}

STGSOEMessage *stg_soe_message_power_set(SoupURI *base, gboolean on) {
    g_autoptr(JsonObject) object = stg_jse_builder_power_set(on);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(STGJSEGenerator) generator = stg_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1.6/power");

    STGSOEMessage *self = g_object_new(STG_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean stg_soe_message_power_set_finish(STGSOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code != SOUP_STATUS_NO_CONTENT) {
        (void)stg_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

STGSOEMessage *stg_soe_message_channels_get(SoupURI *base) {
    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1.6/channels");

    STGSOEMessage *self = g_object_new(STG_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_GET, "uri", uri, NULL);
    return self;
}

gboolean stg_soe_message_channels_get_finish(STGSOEMessage *self, GList **channels, GError **error) {
    if (SOUP_MESSAGE(self)->status_code == SOUP_STATUS_OK) {
        g_autoptr(STGJSEParser) parser = stg_jse_parser_new();
        if (!json_parser_load_from_data(JSON_PARSER(parser), SOUP_MESSAGE(self)->response_body->data, SOUP_MESSAGE(self)->response_body->length, error)) return FALSE;
        JsonNode *node = json_parser_get_root(JSON_PARSER(parser));

        JsonArray *array = json_node_get_array(node);
        g_autolist(STGChannel) ret_channels = stg_jse_reader_channels_get(array);
        GE_SET_VALUE(channels, g_steal_pointer(&ret_channels));
    } else {
        (void)stg_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}

STGSOEMessage *stg_soe_message_channel_set(SoupURI *base, STGChannel *channel) {
    g_autoptr(JsonObject) object = stg_jse_builder_channel_set(channel);
    g_autoptr(JsonNode) node = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(node, object);

    g_autoptr(STGJSEGenerator) generator = stg_jse_generator_new(node);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    g_autoptr(SoupURI) uri = soup_uri_new_with_base(base, "v1.6/channels/current");

    STGSOEMessage *self = g_object_new(STG_TYPE_SOE_MESSAGE, "method", SOUP_METHOD_PUT, "uri", uri, NULL);
    soup_message_set_request(SOUP_MESSAGE(self), JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    return self;
}

gboolean stg_soe_message_channel_set_finish(STGSOEMessage *self, GError **error) {
    if (SOUP_MESSAGE(self)->status_code != SOUP_STATUS_ACCEPTED) {
        (void)stg_soe_message_set_error(self, error);
        return FALSE;
    }

    return TRUE;
}
