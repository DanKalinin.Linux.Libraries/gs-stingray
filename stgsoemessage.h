//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGSOEMESSAGE_H
#define LIBRARY_GS_STINGRAY_STGSOEMESSAGE_H

#include "stgmain.h"
#include "stgschema.h"
#include "stgjsebuilder.h"
#include "stgjsegenerator.h"
#include "stgjsereader.h"
#include "stgjseparser.h"

G_BEGIN_DECLS

#define STG_TYPE_SOE_MESSAGE stg_soe_message_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGSOEMessage, stg_soe_message, STG, SOE_MESSAGE, SOEMessage)

struct _STGSOEMessageClass {
    SOEMessageClass super;
};

struct _STGSOEMessage {
    SOEMessage super;
};

STGSOEMessage *stg_soe_message_power_set(SoupURI *base, gboolean on);
gboolean stg_soe_message_power_set_finish(STGSOEMessage *self, GError **error);

STGSOEMessage *stg_soe_message_channels_get(SoupURI *base);
gboolean stg_soe_message_channels_get_finish(STGSOEMessage *self, GList **channels, GError **error);

STGSOEMessage *stg_soe_message_channel_set(SoupURI *base, STGChannel *channel);
gboolean stg_soe_message_channel_set_finish(STGSOEMessage *self, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGSOEMESSAGE_H
