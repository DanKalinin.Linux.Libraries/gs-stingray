//
// Created by root on 14.11.2021.
//

#include "stgsoesession.h"

void stg_soe_session_finalize(GObject *self);

G_DEFINE_TYPE(STGSOESession, stg_soe_session, SOE_TYPE_SESSION)

static void stg_soe_session_class_init(STGSOESessionClass *class) {
    G_OBJECT_CLASS(class)->finalize = stg_soe_session_finalize;
}

static void stg_soe_session_init(STGSOESession *self) {

}

void stg_soe_session_finalize(GObject *self) {
    stg_soe_session_set_base(STG_SOE_SESSION(self), NULL);

    G_OBJECT_CLASS(stg_soe_session_parent_class)->finalize(self);
}

STGSOESession *stg_soe_session_new(SoupURI *base, gchar *user_agent) {
    STGSOESession *self = g_object_new(STG_TYPE_SOE_SESSION, "accept-language-auto", TRUE, "timeout", 5, "user-agent", user_agent, NULL);

    stg_soe_session_set_base(self, base);

    STGSOELogger *logger = stg_soe_logger_new();
    soup_session_add_feature(SOUP_SESSION(self), SOUP_SESSION_FEATURE(logger));

    return self;
}

gboolean stg_soe_session_power_set_sync(STGSOESession *self, gboolean on, GError **error) {
    g_autoptr(STGSOEMessage) message = stg_soe_message_power_set(self->base, on);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    gboolean ret = stg_soe_message_power_set_finish(message, error);
    return ret;
}

gboolean stg_soe_session_channels_get_sync(STGSOESession *self, GList **channels, GError **error) {
    g_autoptr(STGSOEMessage) message = stg_soe_message_channels_get(self->base);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    gboolean ret = stg_soe_message_channels_get_finish(message, channels, error);
    return ret;
}

gboolean stg_soe_session_channel_set_sync(STGSOESession *self, STGChannel *channel, GError **error) {
    g_autoptr(STGSOEMessage) message = stg_soe_message_channel_set(self->base, channel);
    (void)soup_session_send_message(SOUP_SESSION(self), SOUP_MESSAGE(message));
    gboolean ret = stg_soe_message_channel_set_finish(message, error);
    return ret;
}
