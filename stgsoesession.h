//
// Created by root on 14.11.2021.
//

#ifndef LIBRARY_GS_STINGRAY_STGSOESESSION_H
#define LIBRARY_GS_STINGRAY_STGSOESESSION_H

#include "stgmain.h"
#include "stgsoelogger.h"
#include "stgsoemessage.h"

G_BEGIN_DECLS

#define STG_TYPE_SOE_SESSION stg_soe_session_get_type()

GE_DECLARE_DERIVABLE_TYPE(STGSOESession, stg_soe_session, STG, SOE_SESSION, SOESession)

struct _STGSOESessionClass {
    SOESessionClass super;
};

struct _STGSOESession {
    SOESession super;

    SoupURI *base;
};

GE_STRUCTURE_FIELD(stg_soe_session, base, STGSOESession, SoupURI, soup_uri_copy, soup_uri_free, NULL)

STGSOESession *stg_soe_session_new(SoupURI *base, gchar *user_agent);

gboolean stg_soe_session_power_set_sync(STGSOESession *self, gboolean on, GError **error);
gboolean stg_soe_session_channels_get_sync(STGSOESession *self, GList **channels, GError **error);
gboolean stg_soe_session_channel_set_sync(STGSOESession *self, STGChannel *channel, GError **error);

G_END_DECLS

#endif //LIBRARY_GS_STINGRAY_STGSOESESSION_H
